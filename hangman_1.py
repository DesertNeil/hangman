# user inputs a word to play

word = input('Please enter your word: ')
word = word.lower()
word_length = len(word)

underscores = ['_' for x in range(word_length)]

print(underscores)

i = 0
while i < 6:
    guess_check = input('Would you like to guess the word? (Y/N): ')
    guess_check = guess_check.lower()
    if guess_check == 'n':
        letter_guess = input('Enter a single letter for your guess: ')
        letter_guess = letter_guess.lower()
        if letter_guess in word:
            print(f'Nice! Looks like we found {letter_guess} in the word')
            position = ([pos for pos, char in enumerate(word) if char == letter_guess])
            for index in position:
                underscores[index] = letter_guess
            print(underscores)

        else:
            i += 1
            print(f"Sorry, looks like there are no {letter_guess}'s in this word. You have {6-i} guesses remaining.")
    else:
        word_guess = input('Please enter your guess: ')
        if word_guess == word:
            print('Nice, you guessed the word!')
            break
        else:
            i += 1
            print(f"Sorry, that's not it. You have {6-i} guesses remaining.")
            continue

print("Thanks for playing!")